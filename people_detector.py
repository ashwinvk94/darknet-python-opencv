import cv2
import sys, os
sys.path.append(os.path.join(os.getcwd(),'python/'))

from darknet import *
import pdb
import cv2

def array_to_image(arr):
    arr = arr.transpose(2,0,1) 
    c = arr.shape[0]
    h = arr.shape[1]
    w = arr.shape[2]
    arr = (arr/255.0).flatten()
    data = c_array(c_float, arr)
    im = IMAGE(w,h,c,data)
    return im

set_gpu(0)
#net = load_net("cfg/yolov3.cfg", "weights/yolov3.weights", 0)
net = load_net("cfg/yolov2-tiny.cfg", "weights/yolov2-tiny.weights", 0)

meta = load_meta("cfg/coco.data")

cap = cv2.VideoCapture(1)

while 1:
	for k in range(5):
		ret, frame = cap.read()
	#im =cv2.imread('data/dog.jpg')
	im=frame
	image = array_to_image(im)

	rgbgr_image(image)

	r = detect(net, meta, image)
	print r
	for item in r:
		if item[0]=='person':
			print(item[2])
			x = int(item[2][0])
			y = int(item[2][1])
			width = int(item[2][2])
			height = int(item[2][3])
			cv2.rectangle(im, (x-int(width/2), y-int(height/2)), (x+int(width/2), y+int(height/2)), (255,0,0), 2)
	cv2.imshow('person_detector',im)
	k = cv2.waitKey(0)
	if k==27:
		break
	else:
		cv2.destroyAllWindows()